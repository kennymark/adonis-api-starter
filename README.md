# Backend API

This is a complete, all in one solution for starting a simple fullstack rest web api. It inclues all the necessary functionality one might need to start a simple api

## Features

- User signin
- User signup
- Account Confirmation
- Delete account
- Emails with SendGrid or Mailgun
- Authentization
- Authorization
- ES6 && ESNext
- Input Validation
- Reset Password / Forgetten Password
- MVC Pattern
- Written in AdonisJS
- Event and Listeners for Mail

## Todo

- [ ] Implement Redis for caching
- [ ] Teams feature
- [ ] Payments
