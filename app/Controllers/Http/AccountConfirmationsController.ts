import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/user'

export default class AccountConfirmationsController {
  async index({ response, params, logger, request }: HttpContextContract) {
    if (!request.hasValidSignature()) {
      return response.notAcceptable({ message: 'Token has either expired or is invalid' })
    }

    try {
      const user = (await User.findBy('email', params?.email)) as User

      if (user.confirmed == 1) {
        return { message: 'Your account has already been confirmed' }
      } else {
        user.confirmed = 1
        await user?.save()

        logger.info('account confirmed', user)

        return { message: 'Your account has been succesfully confirmed' }
      }
    } catch (error) {
      logger.error(error)

      return response.badRequest({ message: 'No such email could be found or error occurred' })
    }
  }
}
