import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import LoginValidator from 'App/Validators/LoginValidator'
import Messages from 'App/Validators/Messages'
import RegisterValidator from 'App/Validators/RegisterValidator'
import User from 'App/Models/user'
import Event from '@ioc:Adonis/Core/Event'
import Route from '@ioc:Adonis/Core/Route'
import { frontEndifyUrl, dev_url } from 'App/Utils/main'

export default class AuthController {
  async login({ request, auth, response }: HttpContextContract) {
    const { email, password, is_mobile } = await request.validate(LoginValidator)
    const token = await auth.attempt(email, password, { expiresIn: '2h' })
    const mobileToken = is_mobile && (await auth.attempt(email, password))

    if (!auth.user?.confirmed) {
      return response.badRequest({ errors: [{ message: Messages.account_not_confirmed }] })
    }

    return {
      ...token.toJSON(),
      mobileToken: mobileToken?.toJSON(),
      message: Messages.login_success,
      user: auth.user?.toJSON(),
    }
  }

  async register({ request, response, logger }: HttpContextContract) {
    try {
      const data = await request.validate(RegisterValidator)
      const user = await User.create(data)
      this.sendConfirmationMail(user)
      return { message: Messages.account_created }
    } catch (error) {
      logger.error('signup fail', error)
      return response.badRequest(error?.messages)
    }
  }

  async sendConfirmationMail(user: User) {
    const link = Route.makeSignedUrl('confirmation', {
      params: { email: user.email },
      expiresIn: '2h',
    })

    const parsedUrl = dev_url + frontEndifyUrl(link!)
    Event.emit('new:user', { user, parsedUrl })
  }

  async resendConfirmationMail({ params, response, logger }: HttpContextContract) {
    console.log('resending', params.email)
    try {
      const user = await User.findBy('email', params.email)
      this.sendConfirmationMail(user as User)
      if (!user) response.notFound({ message: Messages.account_not_found })
      return { message: 'Email Sent' }
    } catch (error) {
      logger.error('email confirmation err', error)
      return response.badRequest(error)
    }
  }

  async logout({ auth }: HttpContextContract) {
    await auth.logout()
    return { message: 'Logged out successfully' }
  }
}
