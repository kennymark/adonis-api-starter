import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/user'
import Route from '@ioc:Adonis/Core/Route'
import Messages from 'App/Validators/Messages'
import Event from '@ioc:Adonis/Core/Event'
import { frontEndifyUrl } from 'App/Utils/main'

export default class PasswordResetsController {
  async forgotPassword({ request, response }: HttpContextContract) {
    try {
      const user = await User.findByOrFail('email', request.input('email'))

      const signedUrl = Route.makeSignedUrl('forgotPassword', {
        params: { email: user?.email },
        expiresIn: '1h',
      }) as string

      const emailData = { user, signedUrl: frontEndifyUrl(signedUrl) }
      user.resetToken = signedUrl
      await user.save()

      Event.emit('new:password-request', emailData)
      return { signedUrl: emailData.signedUrl }
    } catch (error) {
      console.log(error)
      return response.badRequest({ error, message: Messages.user_not_found })
    }
  }

  async resetPassword({ request, params, response }: HttpContextContract) {
    try {
      if (request.hasValidSignature()) {
        const user = (await User.findBy('email', params.email)) as User
        if (user.resetToken) {
          user.password = request.input('password')
          user.resetToken = ''
          await user?.save()
          Event.emit('password:changed', { ...user.toJSON() })
          return { message: 'Password has been sucesfully changed' }
        } else return response.badRequest({ message: Messages.password_reset_fail })
      } else return response.unauthorized({ message: 'Link has expired' })
    } catch (error) {
      return response.badRequest({ message: error })
    }
  }
}
