import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Team from 'App/Models/Team'

export default class TeamsController {
  async retrieveTeam() {}

  async createTeam({ request, response, auth }: HttpContextContract) {
    const { name } = request.all()
    try {
      const team = await Team.create({ name, email: auth.user?.email })
      return {
        team,
        message: 'team created',
      }
    } catch (error) {
      response.badRequest({ error, message: 'Team could be created' })
    }
  }

  async deleteTeam({ request }: HttpContextContract) {
    const id = request.input('id')
    const team = await Team.find(id)

    await team?.delete()
    return {
      message: 'team deleted',
    }
  }

  async inviteMember({ request }: HttpContextContract) {
    const emails = request.input('email')

    return { emails }
  }

  async deleteMember({}: HttpContextContract) {}
}
