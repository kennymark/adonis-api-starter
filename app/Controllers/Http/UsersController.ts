import Messages from 'App/Validators/Messages'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/user'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'
import Hash from '@ioc:Adonis/Core/Hash'
import Event from '@ioc:Adonis/Core/Event'

export default class UsersController {
  async getAllUsers() {
    return (await User.query().paginate(1, 10)).toJSON()
  }

  async getCurrentTeam({ response }: HttpContextContract) {
    return response.send('Test come ')
  }

  async updateUser({ request }: HttpContextContract) {
    const { email, oldEmail, name } = request.all()
    const user = await User.findByOrFail('email', oldEmail)
    user.email = email
    user.name = name
    await user.save()
    return { message: Messages.user_updated, user: user.toJSON() }
  }

  async updatePassword({ request, response }: HttpContextContract) {
    const { current_password, new_password, email } = request.all()
    const user = await User.findByOrFail('email', email)
    const isValid = await Hash.verify(user.password, current_password)

    if (isValid) {
      user.password = new_password
      await user.save()
      Event.emit('password:updated', user)
      return { message: Messages.password_update_success }
    } else {
      return response.forbidden({ message: Messages.password_update_fail })
    }
  }

  async deleteUser({ response, params }: HttpContextContract) {
    const id = params.id

    try {
      await User.find(id)
      // await user.delete()
      return { message: Messages.account_deleted }
    } catch (error) {
      return response.badRequest({ message: Messages.general_error })
    }
  }

  async getAppHealth({ response }: HttpContextContract) {
    const report = await HealthCheck.getReport()
    return report.healthy ? response.ok(report) : response.badRequest(report)
  }
}
