import { EventsList } from "@ioc:Adonis/Core/Event";
import Mail from "@ioc:Adonis/Addons/Mail";
import Env from "@ioc:Adonis/Core/Env";

const email = Env.get("DEFAULT_EMAIL") as string;

export default class Email {
  public async sendConfirmationEmail(data: EventsList["new:user"]) {
    await Mail.preview((message) => {
      message
        .subject("Please confirm your email")
        .from(email, "Jayson Miller")
        .to(data.user.email)
        .htmlView("emails/confirmation", {
          user: data.user.toJSON(),
          link: data.parsedUrl,
        });
    });
  }

  public async sendWelcomeEmail(user: EventsList["new:user"]) {
    await Mail.preview((message) => {
      message
        .subject("Welcome Aboard")
        .from(email, "Jayson Miller")
        .to(user.email)
        .htmlView("emails/welcome/index", { user });
    });
  }

  public async sendPasswordResetEmail({
    user,
    signedUrl: link,
  }: EventsList["new:password-request"]) {
    await Mail.preview((message) => {
      message
        .subject("You have requested a password change")
        .from(email, "Jayson Miller")
        .to(user.email)
        .htmlView("emails/password/request", { user, link });
    });
  }

  public async passwordChangedEmail(user: EventsList["password:changed"]) {
    await Mail.preview((message) => {
      message
        .subject("Your password has been successfully changed")
        .from(email, "Jayson Miller")
        .to(user.email)
        .htmlView("emails/password/changed", { user });
    });
  }

  public async passwordUpdatedEmail(user: EventsList["password:updated"]) {
    await Mail.preview((message) => {
      message
        .subject("Your password has been successfully updated")
        .from(email, "Jayson Miller")
        .to(user.email)
        .htmlView("emails/password/updated", { user });
    });
  }
}
