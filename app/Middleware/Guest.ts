import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'


export default class Guest {
  public async handle(ctx: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    if (ctx.auth.isLoggedIn) {
      // ctx.session.flash({ error: 'You need to be logged out to view page' })
      ctx.response.unauthorized({ error: 'You need to be logged out to view page' })
    }
    await next()
  }
}
