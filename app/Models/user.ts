import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  HasMany,
  hasMany,
} from '@ioc:Adonis/Lucid/Orm'
import ApiToken from './ApiToken'

export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column() name: string

  @column() email: string

  @column({ serializeAs: null })
  public password: string

  @column({ serializeAs: null }) rememberMeToken: string;

  @column({ serializeAs: null }) resetToken: string

  @column() confirmed: number

  @hasMany(() => ApiToken)
  public tokens: HasMany<typeof ApiToken>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  // @beforeFind()
  // @beforeFetch()
  // public static ignoreDeleted(query: ModelQueryBuilderContract<typeof User>): void {
  //   query.whereNull('deleted_at')
  // }

  // @beforeDelete()
  // public static async softDeletion(user: User): Promise<never> {
  //   user.deletedAt = DateTime.local()
  //   await user.save()

  //   throw 'Soft delete is successful'
  // }

  // public async delete(): Promise<void> {
  //   try {
  //     await super.delete()
  //   } catch (message) {
  //     this.$isDeleted = true
  //   }
  // }
}
