import Env from '@ioc:Adonis/Core/Env'
import Application from '@ioc:Adonis/Core/Application'

const dev_url = Env.get('DEV_URL') as string
const prod_url = Env.get('PROD_URL') as string
const isDev = Application.inDev


function frontEndifyUrl(url: string) {
  const regex = /\/api\/account/gm;
  return url.replace(regex, '')
}

export { frontEndifyUrl, dev_url, prod_url, isDev }