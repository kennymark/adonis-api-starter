import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Messages from './Messages'

export default class LoginValidator {
  constructor(private ctx: HttpContextContract) { }

  public schema = schema.create({
    email: schema.string({ trim: true },
      [rules.email(), rules.maxLength(100), rules.minLength(5), rules.exists({ table: 'users', column: 'email' })]),
    password: schema.string({}, [rules.minLength(4)]),
    is_mobile: schema.boolean.optional()
  })

  public cacheKey = this.ctx.routeKey

  public messages = {
    email: Messages.invalid_email,
    exists: '{{ field }} does not exist',
    minLength: '{{ field }} should be greater than 5 characters'
  }
}
