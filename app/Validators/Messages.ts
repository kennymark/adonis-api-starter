export default {
  account_created: 'Your account has been sucessfully created. Please check your email to activate your account',
  account_deleted: 'Your account has been sucessfully deleted',
  account_not_found: 'There is no account associated with the email',
  account_not_confirmed: 'Your account has not yet been confirmed, please check your email and confirm your account before proceeding',
  admin_only: 'Page is restricted to admins only',
  login_success: 'You have been sucessfully logged in',
  login_failure: 'Error occurred whilst logging in, please check password or try again later',
  invalid_email: 'The email is invalid',
  invalid_password: 'The password is invalid',
  cant_access_resource: 'Please login to view this resource',
  cant_reuse_password: 'The same password cannot be used more than once',
  password_reset_success: 'Congratulations, your password has been reset, you can now log in',
  password_reset_fail: 'Password has already been changed with this token. Please request a new password',
  password_update_success: 'Congratulations, your password has been updated',
  password_update_fail: 'The current password provided is incorrect. Please try again',
  general_error: 'An error has occurred, please try again later',
  logout_to_view: 'You can only access this resource if logged out',
  user_not_found: 'User does not have an account registered. Please signup in order to login or reset password',
  user_updated: 'User has been sucessfully updated',
  user_update_error: 'Could not update user, error occurred, please try again later',

  minLength(field, minLength = 5) {
    return `${field} should be ${minLength} characters or more`
  },

  maxLength(field, maxLength = 100) {
    return `${field} should not be greather than ${maxLength} characters`
  },

  userAlreadyExists(user) {
    return `User ${user.email} already has been registered, login or reset password`
  },

  passwordResetSuccess(user) {
    return `The link to reset your password has been sent to ${user.email}, please check your email.`
  },

  passwordForgotFail(user) {
    return `No such email ${user.email} exists in our database, please register for an account.`
  },

  reset_token_expired: 'The provided reset token has expired, please request a new password',

}