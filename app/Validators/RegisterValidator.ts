import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules, } from '@ioc:Adonis/Core/Validator'
import Messages from './Messages'

export default class RegisterValidator {
  constructor(private ctx: HttpContextContract) { }

  public schema = schema.create({
    email: schema.string({}, [rules.email(), rules.unique({ table: 'users', column: 'email' })]),
    name: schema.string({}, [rules.minLength(5), rules.maxLength(50)]),
    password: schema.string({ trim: true }, [rules.minLength(4), rules.maxLength(50), rules.confirmed()]),
  })

  public cacheKey = this.ctx.routeKey

  public messages = {
    unique: Messages.userAlreadyExists(this.ctx.request.all()),
    'name.minLength': Messages.minLength('Name', 5),
    minLength: Messages.minLength('Password', 4),
    maxLength: `${Messages.maxLength('{{ field }}', 100)}`,
    confirm: Messages.invalid_password
  }
}
