import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/user'

export default class UserSeeder extends BaseSeeder {
  public static developmentOnly = false

  public async run() {
    await User.createMany([
      { name: 'Test Account', email: 'test@test.com', password: 'test', confirmed: 1 },
      { name: 'Kenny Man', email: 'kennymark@test.com', password: 'kennymark', confirmed: 1 },
      { name: 'User 1', email: 'user1@test.com', password: 'user1', confirmed: 1 },
      { name: 'Admin', email: 'admin@server.com', password: 'jake1', confirmed: 1 },
    ])
  }
}
