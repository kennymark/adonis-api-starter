
import Event from '@ioc:Adonis/Core/Event';
import Mail from '@ioc:Adonis/Addons/Mail';
import Logger from '@ioc:Adonis/Core/Logger'

import Database from '@ioc:Adonis/Lucid/Database'
import Application from '@ioc:Adonis/Core/Application'

Event.on('new:user', 'Email.sendConfirmationEmail')
Event.on('new:password-request', 'Email.sendPasswordResetEmail')
Event.on('password:changed', 'Email.passwordChangedEmail')
Event.on('password:updated', 'Email.passwordUpdatedEmail')



Event.on('adonis:mail:sent', Mail.prettyPrint)


Event.on('db:query', (query) => {

  if (Application.inProduction) {
    Logger.debug(query)
  } else {
    Database.prettyPrint(query)
  }
})