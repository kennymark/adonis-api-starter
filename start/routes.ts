import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => ({ message: 'hello world from the adonis api' }))

Route.group(() => {
  Route.get('/', async () => ({ message: 'hello world from the adonis api' }))

  Route.group(() => {
    Route.post('signup', 'AuthController.register')
    Route.post('signin', 'AuthController.login')
    Route.post('signout', 'AuthController.logout')
    Route.post('reauthenticate', 'AuthController.reauthenticate')
    Route.post('confirmation/:email', 'AccountConfirmationsController.index').as('confirmation')

    Route.post('resend-confirmation/:email', 'AuthController.resendConfirmationMail')

    Route.post('forgot-password', 'PasswordResetsController.forgotPassword')
    Route.post('reset-password/:email?', 'PasswordResetsController.resetPassword').as(
      'forgotPassword'
    )

    Route.group(() => {
      Route.get('all', 'UsersController.getAllUsers')
      Route.get('current', 'UsersController.getCurrentUser')
      Route.put('current', 'UsersController.updateUser')
      Route.delete('current/:id', 'UsersController.deleteUser')
      Route.put('current/update-password', 'UsersController.updatePassword')
      Route.get('current/team', 'UsersController.getCurrentTeam')
    })
      .prefix('user')
      .middleware('auth')
  }).prefix('account')

  Route.get('health', 'UsersController.getAppHealth')
}).prefix('api')
