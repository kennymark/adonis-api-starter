/*
|--------------------------------------------------------------------------
| Preloaded File
|--------------------------------------------------------------------------
|
| Any code written inside this file will be executed during the application
| boot.
|
*/

import View from '@ioc:Adonis/Core/View'
import { isDev } from 'App/Utils/main';

View.global('timestamp', () => {
  return new Date().getTime()
})

View.global('baseUrl', () => {
  return isDev ? 'http://localhost:3000' : 'https://next-adonis.vercel.app'
})